let Diaporama = function(json, idIMG) {
	this.json = json;
	this.idIMG = document.getElementById(idIMG);
};

let diaporama1 = new Diaporama("./diapo.json", "baliseIMG");

const requestURL = diaporama1.json;
const request = new XMLHttpRequest();
request.open("GET", requestURL);
request.responseType = "text";
request.send();

request.onload = function() {
	const diapo = request.response;
	const diapoParsed = JSON.parse(diapo);
	const diapoKeys = Object.keys(diapoParsed);

	console.log(diapo);
	console.log(diapoParsed);
	console.log(diapoKeys);

	let i = 0;
	function changeImage() {
		diaporama1.idIMG.setAttribute("src", diapoParsed[diapoKeys[i]]);
		diaporama1.idIMG.setAttribute("alt", diapoKeys[i]);
		i === diapoKeys.length - 1 ? (i = 0) : i++;
	}

	setInterval(changeImage, 1500);
};
