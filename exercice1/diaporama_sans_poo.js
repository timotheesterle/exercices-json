const baliseIMG = document.getElementById("baliseIMG");

const requestURL = "./diapo.json";
const request = new XMLHttpRequest();
request.open("GET", requestURL);
request.responseType = "text";
request.send();

request.onload = function() {
	const diapo = request.response;
	const diapoParsed = JSON.parse(diapo);
	const diapoKeys = Object.keys(diapoParsed);

	console.log(diapo);
	console.log(diapoParsed);
	console.log(diapoKeys);

	let i = 0;
	function changeImage() {
		baliseIMG.setAttribute("src", diapoParsed[diapoKeys[i]]);
		baliseIMG.setAttribute("alt", diapoKeys[i]);
		i === diapoKeys.length - 1 ? (i = 0) : i++;
	}

	setInterval(changeImage, 1500);
};
